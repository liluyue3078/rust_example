# rust 学习过程中的一些代码样例
这个只作学习、探索一些东西的 代码存放，以便快速实践,无实际用途
## macro 

### 声明宏
[语法文档](https://doc.rust-lang.org/reference/macros-by-example.html)

1. macro_rules_one.rs
2. macro_rules_two.rs
3. macro_rules_three.rs
4. macro_rules_four.rs
5. macro_rules_five.rs
6. macro_rules_sive.rs
7. macro_rules_seven.rs

```
    block
    expr 用于表达式
    ident 用于变量名或函数名
    item
    literal 用于字面常量
    pat (模式 pattern)
    path
    stmt (语句 statement)
    tt (标记树 token tree)
    ty (类型 type)
    vis (可见性描述符)
```

### Option
1. option.rs

### List 

[学习资料来源](https://rust-unofficial.github.io/too-many-lists/index.html)

1. link_first.rs :简单链表结构
2. link_second.rs :补全迭代功能
3. link_third.rs :不可变链表
4. link_fourth.rs :不可变链表
5. link_fifth.rs : 生命期笨拙演示
6. link_sive.rs : 不合格的unsafe演示
7. link_seventh.rs : 合格的unsafe演示
8. link_eighth.rs : 双端链表
8. link_silly1.rs : 安全代码下的单链接队列实现思路

### Rustonomicon

[资料来源](https://doc.rust-lang.org/nomicon/index.html)

1. rustonomicon/vec.rs : Vec实现
2. rustonomicon/arc.rs : Arc实现
