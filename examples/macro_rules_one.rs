macro_rules! create_func {
    ($func_name:ident) => {
        fn $func_name() {
            println!("called funcation:{}", stringify!($func_name));
        }
    };
}
create_func!(func_one);
create_func!(func_two);
fn main() {
    func_one();
    func_two();
}
