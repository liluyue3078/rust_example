// `min!` 将求出任意数量的参数的最小值。
macro_rules! find_min {
    ($x:expr) => {
        $x
    };
// $(...),+ 包围起来，就可以匹配一个或多个用逗号隔开的表达式
    ($x:expr, $($y:expr),+) =>(
        std::cmp::min($x,find_min!($($y),+))
    );
}
fn main() {
    println!("{}", find_min!(2));
    println!("{}", find_min!(2, 1i32 + 2));
    println!("{}", find_min!(1, 1i32 + 2, 4i32));
}
