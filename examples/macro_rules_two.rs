macro_rules! mock_dbg {
    ($express:expr) => {
        println!("express: {:?} = {:?}", stringify!($express), $express);
    };
}

fn main() {
    mock_dbg!(1 + 2);
    mock_dbg!({
        let mut a = "a".to_string();
        a.push('b');
        a
    });
}
