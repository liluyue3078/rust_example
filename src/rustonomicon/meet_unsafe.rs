/// 错误的例子，用于说明不遵从unsafe 函数自身约定的危害
fn index(idx: usize, arr: &[u8]) -> Option<u8> {
    unsafe { Some(*arr.get_unchecked(idx)) }
}

#[cfg(test)]
mod test {
    use super::index;

    #[test]
    fn meet_unsafe() {
        let arr = [];
        let a = index(11, &arr);
        //    empty数组的索引有值，说明get_unchecked()做了越界访问
        assert!(a.is_some());
    }
}
