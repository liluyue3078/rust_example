use std::mem::{self, MaybeUninit};
fn create_arr() -> [Box<i32>; 10] {
    const size: usize = 10;
    let x = {
        let mut x: [MaybeUninit<Box<i32>>; size] = unsafe { MaybeUninit::uninit().assume_init() };
        for i in 0..size {
            x[i] = MaybeUninit::new(Box::new(i as i32));
        }
        unsafe { mem::transmute::<_, [Box<i32>; size]>(x) }
    };
    x
}
fn create_arr_by_write() -> [Box<i32>; 10] {
    const size: usize = 10;
    let x = {
        let mut x: [MaybeUninit<Box<i32>>; size] = unsafe { MaybeUninit::uninit().assume_init() };
        for i in 0..size {
            let val = i as i32;
            let val = Box::new(val);
            x[i].write(val);
        }
        unsafe { mem::transmute::<_, [Box<i32>; size]>(x) }
    };
    x
}
struct Demo(bool, i32);
fn createDemo() -> Demo {
    let mut demo = MaybeUninit::<Demo>::uninit();
    let field_first_ptr = unsafe { std::ptr::addr_of_mut!((*demo.as_mut_ptr()).0) };
    let field_second_ptr = unsafe { std::ptr::addr_of_mut!((*demo.as_mut_ptr()).1) };
    unsafe {
        field_first_ptr.write(true);
        field_second_ptr.write(10);
        demo.assume_init()
    }
}
#[cfg(test)]
mod test {
    use crate::rustonomicon::memory::create_arr_by_write;

    use super::{createDemo, create_arr};

    #[test]
    fn base() {
        let arr = create_arr();
        let expected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(|f| Box::new(f));
        assert_eq!(arr, expected);
        assert_eq!(create_arr_by_write(), expected);
    }
    #[test]
    fn second() {
        let demo = createDemo();
        assert_eq!(demo.0, true);
        assert_eq!(demo.1, 10);
    }
}
