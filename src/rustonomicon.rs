pub mod arc;
/// referfence 指定了rust的句法和语法，但是在实际操作中知识组合在一起会有各种问题，
/// rustonomicon 旨在通过例子才说明rust内在的东西 。
/// unsafe旨在指明谁应该为代码的不安全负责，谁应该遵循那些compiler无法维护的一些合约。
pub mod meet_unsafe;
pub mod memory;
pub mod vec;
