use std::mem;

struct Node<T> {
    element: T,
    next: Link<T>,
}
type Link<T> = Option<Box<Node<T>>>;
pub struct List<T> {
    head: Link<T>,
}
impl<T> List<T> {
    pub fn new() -> Self {
        List { head: None }
    }
    pub fn push(&mut self, item: T) {
        self.head = Some(Box::new(Node {
            element: item,
            // 可变性与共享是互斥的，所以我们不能再改变head的同时使用head，
            // 或者在head数据缺失的情况下操纵head，
            // 所以这里用mem::replace()先剥离head的共享行为
            next: mem::replace(&mut self.head, None),
        }));
    }
    pub fn pop(&mut self) -> Option<T> {
        let node: Box<Node<T>> = mem::replace(&mut self.head, None)?;
        // 获取一个不在使用的所有者的部分值，不需要所有者的可变性
        self.head = node.next;
        Some(node.element)
    }
}
impl<T> Drop for List<T> {
    fn drop(&mut self) {
        while let Some(h) = mem::replace(&mut self.head, None) {
            self.head = h.next;
            //    println!("{:?}",h.element);
        }
    }
}
#[cfg(test)]
mod test {
    use super::List;

    #[test]
    fn basics() {
        let mut list: List<i32> = List::new();
        assert_eq!(list.pop(), None);
        list.push(1);
        list.push(2);
        list.push(3);
        assert_eq!(list.pop(), Some(3));
        assert_eq!(list.pop(), Some(2));
        assert_eq!(list.pop(), Some(1));
        assert_eq!(list.pop(), None);
    }
}
